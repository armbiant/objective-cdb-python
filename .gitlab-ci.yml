#################################################################
#                                                               #
# Copyright (c) 2020-2021 Peter Goss All rights reserved.       #
#                                                               #
# Copyright (c) 2020-2023 YottaDB LLC and/or its subsidiaries.  #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################


stages:
  - formating-checks
  - test


.ubuntu:
  image: yottadb/yottadb-base:latest-master
  before_script:
    - apt update
    - apt install -y python3-setuptools python3-dev libffi-dev python3-pip pkg-config
    - source $(pkg-config --variable=prefix yottadb)/ydb_env_set
    - python3 -m pip install pytest-runner
    - python3 setup.py install --user

.rocky:
  image: yottadb/yottadb-rocky-base:latest-master
  before_script:
    - yum install -y python3 gcc python3-devel libffi-devel libasan pkg-config
    - source $(pkg-config --variable=prefix yottadb)/ydb_env_set
    - python3 setup.py install --user

.test_ubuntu:
  script:
    # Set LD_PRELOAD and ASAN_OPTIONS in case YottaDB is built with ASAN. If it isn't, these variables will simply be ignored.
    - export LD_PRELOAD=$(gcc -print-file-name=libasan.so)
    - export ASAN_OPTIONS="detect_leaks=0:disable_coredump=0:unmap_shadow_on_exit=1:abort_on_error=1"
    - python3 -m pip install --user pytest pytest-order psutil
    - python3 -m pytest tests/ -vs 2> pytest.err 1> pytest.out
  artifacts:
    paths:
      - ./*.out
      - ./*.err
    when: always

.test_rocky:
  script:
    - export LD_PRELOAD=$(cat $(gcc -print-file-name=libasan.so) | cut -f 3 -d" ")
    - export ASAN_OPTIONS="detect_leaks=0:disable_coredump=0:unmap_shadow_on_exit=1:abort_on_error=1"
    - python3 -m pip install --user pytest pytest-order psutil
    - python3 -m pytest tests/ -vs 2> pytest.err 1> pytest.out
  artifacts:
    paths:
      - ./*.out
      - ./*.err
    when: always

.debug:
  script:
    # Enable Debug build be uncommenting undef_macros compiler line in setup.py
    - sed -i 's/# undef_macros/undef_macros/' setup.py

test_ubuntu:
  stage: test
  extends:
    - .ubuntu
    - .test_ubuntu

test_rocky:
  stage: test
  extends:
    - .rocky
    - .test_rocky

test_ubuntu_debug:
  stage: test
  extends:
    - .ubuntu
    - .debug
    - .test_ubuntu

commit-verify:
  image: ubuntu:18.04
  stage: formating-checks
  before_script:
    - apt update -qq && apt-get install -y -qq git wget gnupg
  script:
    # Copy commit gpg key verify script to build directory and execute
    - wget https://gitlab.com/YottaDB/DB/YDB/-/raw/master/ci/commit_verify.sh
    - chmod +x commit_verify.sh
    - chmod +x ./tools/ci/needs_copyright.sh
    - ./commit_verify.sh ./tools/ci/needs_copyright.sh https://gitlab.com/YottaDB/Lang/YDBPython

code-format-check:
  stage: formating-checks
  extends:
    - .ubuntu
  artifacts:
    paths:
      - temp_warnings.txt
      - clang_tidy_warnings.txt
      - sorted_warnings.txt
    when: always
  script:
    - apt update -qq && apt-get install -y -qq git wget gnupg pkg-config clang-format-11 python3-pip clang-tidy-11
    - source $(pkg-config --variable=prefix yottadb)/ydb_env_set
    - python3 -m pip install black
    - LC_ALL=C.UTF-8 bash ./tools/ci/check_code_format.sh
